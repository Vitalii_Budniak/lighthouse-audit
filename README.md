# Lighthouse-audit

Simple solution with `jest-playwright-preset`, `playwright`, `playwright-lighthouse`

# Documentation

Playwright docs: https://playwright.dev

Playwright-lighthouse docs: https://www.npmjs.com/package/playwright-lighthouse

Jest docs: https://jestjs.io/uk/docs/getting-started

## Getting started

1. Clone repository
2. Install dependencies `npm install`
3. Run test `npm run test`
