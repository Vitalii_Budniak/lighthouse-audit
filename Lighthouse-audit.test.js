const { describe, it, expect } = require ('@jest/globals');

const { playAudit } = require('playwright-lighthouse');
const desktopConfig = require('lighthouse/lighthouse-core/config/desktop-config.js');
const defaultConfig = require('lighthouse/lighthouse-core/config/default-config');
const playwright = require('playwright');

const isForMobileDevice = false;
const device = isForMobileDevice ? defaultConfig : desktopConfig;

describe('Audit example', () => {
  it(`Check Apiko main page for ${isForMobileDevice ? 'mobile' : 'desktop'} mode`, async () => {

    const thresholdsConfig = {
      performance: 80,
      accessibility: 50,
      'best-practices': 50,
      seo: 50,
      pwa: 50,
    };

    const browser = await playwright['chromium'].launch({
      headless: false,
      args: ['--remote-debugging-port=9222'],
    });

    const page = await browser.newPage();
    await page.goto('https://apiko.com');
    await page.waitForTimeout(1000);

    let isError = false;
    try {
      await playAudit({
        page: page,
        port: 9222,
        thresholds: thresholdsConfig,
        // opts: lighthouseOptions,
        config: device,
        reports: {
          formats: {
            json: false,
            html: true,
            csv: false,
          },
          name: `audit-report`, //`audit-report-${new Date().getHours()}:${new Date().getMinutes()}`
          directory: `report`, //defaults to `${process.cwd()}/lighthouse`
        },
      });
    } catch (e) {
      isError = e;
      console.log(e);
    }
    await browser.close();
    expect(isError).toBeFalsy();
  });
});

// (async () => {
//   const thresholdsConfig = {
//     performance: 91,
//     accessibility: 50,
//     'best-practices': 50,
//     seo: 50,
//     pwa: 50,
//   };
//
//   const browser = await playwright['chromium'].launch({
//     headless: true,
//     args: ['--remote-debugging-port=9222'],
//   });
//   const page = await browser.newPage();
//   await page.goto('https://apiko.com');
//   await page.waitForTimeout(2000);
//   try {
//     await playAudit({
//       page: page,
//       port: 9222,
//       thresholds: thresholdsConfig,
//       // opts: lighthouseOptions,
//       config: device,
//       reports: {
//         formats: {
//           json: true,
//           html: true,
//           csv: false,
//         },
//         name: `audit-report`, //`audit-report-${new Date().getHours()}:${new Date().getMinutes()}`
//         directory: `report`, //defaults to `${process.cwd()}/lighthouse`
//       },
//     });
//   }catch (e) {
//     console.log(e);
//   }
//   await browser.close();
// })();
